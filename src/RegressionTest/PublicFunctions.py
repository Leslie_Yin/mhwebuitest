'''
Created on Apr 3, 2016

@author: olivia.xie
'''
# -*- coding: utf-8 -*-


from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium import webdriver
import time, httplib
from PIL import Image
from PIL import ImageGrab
from PIL import ImageChops
import csv
import string  # string.printable
import os
os.chdir("C:/Users/Rider.Wu/git/mhwebuitest/src/RegressionTest")



def test_compare_images(driver1,ele,scale,actual_path):
    expect_path = actual_path("actual",driver1.name+"/expect")
    driver1.execute_script("arguments[0].scrollIntoView(true);", ele)
    im = ImageGrab.grab(scale)
    im.save(actual_path)
            
    actual = Image.open(actual_path,'r')        
    expect = Image.open(expect_path,'r')
    list1 = actual.histogram() #to return the pixel value to each point
    list2 = expect.histogram()
    try:
        assert list1 == list2
    except:
        test_difference_image(expect_path,actual_path)
        
def test_difference_image(a,b):
    im_expect1 = Image.open(a)
    im_actual = Image.open(b)
    im_expect = im_expect1.resize(im_actual.size)  # to make sure expect image and actual image are same size
    point_table = ([0] + ([255] * 255))
    diff = ImageChops.difference(im_expect, im_actual)
    diff = diff.convert('L')
    diff = diff.point(point_table)
    new = diff.convert('RGB')
    new1 = diff.convert("RGB")
    new.paste(im_actual,mask=diff)
    new.save(b.replace(".jpg","_difference_actual.jpg"))
    new1.paste(im_expect,mask=diff)
    new1.save(b.replace(".jpg","_difference_expect.jpg"))

    
    
def  test_image_grab_without_header(driver1,ele,path): 
        size = driver1.get_window_size()
       
        xy = ele.location   # xy is a dic to store the location of dropdown menu
        big = ele.size      # big is a dic to store the size of dropdown menu
        
        x = int(round((xy["x"])))
        y = int(round((xy["y"])))
        h = int(big["height"]) - 1
        w = int(big["width"]) - 1
        
        driver1.execute_script("arguments[0].scrollIntoView(true);", ele)
        time.sleep(2) 
        if driver1.name == 'chrome':
            offset = size['height']-86  # footer height
            Y = y % offset
            
            if y > 5000:
                scale = (x,Y,x+w,h+Y) 
            
            elif ele.get_attribute('class') == 'booking-bar': # for footer
                scale = (x,size['height']-96,x+w,size['height']-96+h)
            
            elif ele.get_attribute('class') == 'booking':
                scale = (x,size['height']-492,x+w,size['height']-492+h)
                
            elif ele.get_attribute('class') == 'booking-widget when-no-options':
                scale = (x,170,x+w,170+h)
            
            else:
                scale = (x,61,x+w,61+h)
                
        elif driver1.name == 'firefox':
            offset = size['height']-86  # footer height
            Y = y % offset
            
            if y > 5000:
                scale = (x,Y,x+w,h+Y) 
            
            elif ele.get_attribute('class') == 'booking-bar': # for footer
                scale = (x,size['height']-96,x+w,size['height']-96+h)
            
            elif ele.get_attribute('class') == 'booking':
                scale = (x,size['height']-492,x+w,size['height']-492+h)
                
            elif ele.get_attribute('class') == 'booking-widget when-no-options':
                scale = (x,205,x+w,205+h)
                
            
            else:
                scale = (x,71,x+w,71+h)
                   
        elif driver1.name == 'internet explorer':
            offset = size['height']-86  # footer height
            Y = y % offset
            
            if y > 5000:
                scale = (x,Y,x+w,h+Y) 
            
            elif ele.get_attribute('class') == 'booking-bar': # for footer
                scale = (x,size['height']-96,x+w,size['height']-96+h)
            
            elif ele.get_attribute('class') == 'booking':
                scale = (x,size['height']-490,x+w,size['height']-490+h)
                
            elif ele.get_attribute('class') == 'booking-widget when-no-options':                 # seamless
                scale = (x,180,x+w,180+h)
                
            else:
                scale = (x,55,x+w,55+h)  
        print scale
        im = ImageGrab.grab(scale)
        im.save(path)  
        img = Image.open(path,'r')
        List = img.histogram()
        return List
    
def  test_compare_image_without_header(driver1,ele,actual_path): 
        Flag = False
        #  get the browser height and footer height
        size = driver1.get_window_size()
        expect_path1 = actual_path.replace("actual","expect") #expect result path
        expect_path = expect_path1[:6] + "/" + driver1.name + expect_path1[6:]

       
        xy = ele.location   # xy is a dic to store the location of dropdown menu
        big = ele.size      # big is a dic to store the size of dropdown menu
        
        x = int(round((xy["x"])))
        y = int(round((xy["y"])))
        h = int(big["height"]) - 1
        w = int(big["width"]) - 1
        
        driver1.execute_script("arguments[0].scrollIntoView(true);", ele)
        time.sleep(2) 
        if driver1.name == 'chrome':

            offset = size['height']-86  # footer height

            Y = y % offset
            if y > 5000:
                scale = (x,Y,x+w,h+Y) 
                
            
            elif ele.get_attribute('class') == 'booking-bar': # for footer
                scale = (x,size['height']-96,x+w,size['height']-96+h)
            
            elif ele.get_attribute('class') == 'booking':
                scale = (x,size['height']-492,x+w,size['height']-492+h)
                
            elif ele.get_attribute('class') == 'booking-widget when-no-options':                 # seamless
                scale = (x,170,x+w,170+h)

            elif ele.get_attribute('class') == 'destination-popup destination-list':
                scale = (x,61,x+w,154+h)  
                          
            else:
                scale = (x,61,x+w,61+h)

        elif driver1.name == 'firefox':
            offset = size['height']-86  # footer height
            Y = y % offset
            if y > 5000:
                scale = (x,Y,x+w,h+Y) 
            
            elif ele.get_attribute('class') == 'booking-bar': # for footer
                scale = (x,size['height']-96,x+w,size['height']-96+h)
            
            elif ele.get_attribute('class') == 'booking':
                scale = (x,size['height']-492,x+w,size['height']-492+h)
                
            elif ele.get_attribute('class') == 'booking-widget when-no-options':
                scale = (x,180,x+w,180+h)
                
            elif ele.get_attribute('class') == 'destination-popup destination-list':
                scale = (x,164,x+w,164+h)
            
            else:
                scale = (x,71,x+w,71+h)
                
        elif driver1.name == 'internet explorer':
            offset = size['height']-86  # footer height

            Y = y % offset
            if y > 5000:
                scale = (x,Y,x+w,h+Y) 
            
            elif ele.get_attribute('class') == 'booking-bar': # for footer
                scale = (x,size['height']-96,x+w,size['height']-96+h)
            
            elif ele.get_attribute('class') == 'booking':
                scale = (x,size['height']-490,x+w,size['height']-490+h)

            elif ele.get_attribute('class') == 'booking-widget when-no-options':                 # seamless
                scale = (x,170,x+w,170+h)
            elif ele.get_attribute('class') == 'destination-popup destination-list':
                scale = (x,148,x+w,148+h)
                
            else:
                scale = (x,55,x+w,55+h)            

        im = ImageGrab.grab(scale)
        im.save(actual_path)  
        actual = Image.open(actual_path,'r')        
        expect = Image.open(expect_path,'r')
        list1 = actual.histogram() #to return the pixel value to each point
        list2 = expect.histogram()

        assert list1 == list2
        Flag = True        
        return Flag

def test_difference_between_image(driver1,actual_path): 
    expect_path1 = actual_path.replace("actual","expect")
    expect_path = expect_path1[:6] + "/" + driver1.name + expect_path1[6:] 
    difference_path = actual_path.replace("actual","difference")
    print expect_path
    im1 = Image.open(actual_path)
    im2 = Image.open(expect_path)
    im3 = ImageChops.invert(im2)
    try:
        im = ImageChops.blend(im1,im3,0.5)
    except:
        im = ImageChops.blend(im1,im3.resize(im1.size),0.5)
    im.save(difference_path)
    print "please refer to " + difference_path + " for more details"
                

def test_page_redirect(url):
    length = len(url)
    req = url[58:length]
    print req
    conn = httplib.HTTPConnection('52.11.180.229',80)  
        
#  conn.set_debuglevel(1)  
# HTTPConnection.request ( method , url [ , body [ , headers ]] )  
    conn.request("GET",req,
            headers = {"Host": "preview.thrive.prod.dpaprod.kpwpce.kp-aws-cloud.org",  
           "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36",  
           "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8"})  
 
  
    respond = conn.getresponse()  # conn.getresponse().read() 
    status_code = respond.status
    conn.close()  

    return status_code


    
def test_present_and_wait_xpath(driver1,xpath1,timing):
    flag = False
    i = 0
    while not flag and i < 10:
        try:
            if WebDriverWait(driver1,timing).until(lambda td: td.find_element_by_xpath(xpath1).is_displayed()):
                flag = True 
                return flag                       
        except:
            pass    
        
        finally:
            i = i + 1 
            time.sleep(1)   

    return flag   

def test_present_and_wait_link(driver1,link1,timing):
    flag = False
    i = 0
    while not flag and i < 10:
        try:
            if WebDriverWait(driver1,timing).until(lambda td: td.find_element_by_link_text(link1).is_displayed()):
                flag = True 
                return flag                       
        except:
            pass    
        
        finally:
            i = i + 1   
            time.sleep(1)  
    return flag
 
   
def test_present_and_wait_id(driver1,id1,timing):
    flag = False
    i = 0
    while not flag and i < 10:
        try:
            if WebDriverWait(driver1,timing).until(lambda td: td.find_element_by_id(id1).is_displayed()):
                flag = True  
                return flag                      
        except:
            pass    
        
        finally:
            i = i + 1
            time.sleep(1) 
    return flag    

    
def test_present_and_wait_name(driver1,name1,timing):
    flag = False
    i = 0
    while not flag and i < 10:
        try:
            if WebDriverWait(driver1,timing).until(lambda td: td.find_element_by_name(name1).is_displayed()):
                flag = True 
                return flag                       
        except:
            pass    
        
        finally:
            i = i + 1     
            time.sleep(1)
    return flag

def test_present_and_wait_class(driver1,class1,timing):
    flag = False
    i = 0
    while not flag and i < 10:
        try:
            if WebDriverWait(driver1,timing).until(lambda td: td.find_element_by_class_name(class1).is_displayed()):
#            if WebDriverWait(driver1,timing).until(EC.element_to_be_clickable((By.CLASS_NAME,class1))):
                flag = True 
                print "element is displayed"
                return flag                       
        except:
            pass
            print "element is not present now"  
        
        finally:            
            i = i + 1   
            time.sleep(1)  
    return flag

def test_present_and_wait_css(driver1,css1,timing):
    flag = False
    i = 0
    while not flag and i < 10:
        try:
            if WebDriverWait(driver1,timing).until(lambda td: td.find_element_by_css_selector(css1).is_displayed()):
                flag = True 
                return flag                       
        except:
            pass
            print "element is not present now"  
        
        finally:            
            i = i + 1   
            time.sleep(1)  
    return flag

# web page redirects to internal or external url;how to use the after_url????????

def test_page_redirect_xpath(driver2,xpath2,after_url,error_info): 

    before_url = driver2.current_url
    before_handle =  driver2.current_window_handle
    flag = False
    flag1 = False
    j = 0

#    error_info = u'This site can\u2019t be reached\nwww.youtube.com took too long to respond.\nERR_CONNECTION_TIMED_OUT\nReload\nDetails'                         
    #  target attribute indicates that page will redirect to external web page
        
    if driver2.find_element_by_xpath(xpath2).get_attribute('target') == u'_blank':
        driver2.find_element_by_xpath(xpath2).click()
        time.sleep(2)   

        while not flag1:
            driver2.switch_to_window(driver2.window_handles[-1])
            if before_handle != driver2.current_window_handle:
                print driver2.current_window_handle
                flag1 = True               
    else:
        driver2.find_element_by_xpath(xpath2).click()
        time.sleep(1) 

    while not flag and j < 10:    
        print driver2.current_url    
        try:

            if driver2.current_url == after_url:
                flag = True
                print 'web page redirects successfully'
                return flag
            
            
            elif driver2.current_url == before_url: 
                 
                print "new page is not present now,wait some seconds" 
              
            elif driver2.find_element_by_xpath("//body").text == error_info:
                flag = True
                print "site cannot be reached"
                return flag
           
        except:
            pass
        finally:
            j = j + 1
            time.sleep(1)

def test_page_redirect_id(driver2,id2,after_url,error_info): 

    before_url = driver2.current_url
    before_handle =  driver2.current_window_handle
    flag = False
    flag1 = False
    j = 0

#    error_info = u'This site can\u2019t be reached\nwww.youtube.com took too long to respond.\nERR_CONNECTION_TIMED_OUT\nReload\nDetails'                         
    #  target attribute indicates that page will redirect to external web page
   
    if driver2.find_element_by_id(id2).get_attribute('target') == u'_blank':
        driver2.find_element_by_id(id2).click()
        time.sleep(2)   
        while not flag1:
            driver2.switch_to_window(driver2.window_handles[-1])
            if before_handle != driver2.current_window_handle:
                flag1 = True               
    
    else:

        driver2.find_element_by_id(id2).click()
        time.sleep(2) 

    while not flag and j < 15:  
     
        try:
            if driver2.current_url == after_url:
                after_url = driver2.current_url
                flag = True
                print 'web page redirects successfully'
                return flag
        
            elif driver2.current_url == before_url:   
                print "new page is not present now,wait some seconds" 
               
            elif driver2.find_element_by_xpath("//body").text == error_info:
                flag = True
                print "site cannot be reached"
                return flag
           
        except:
            pass
        finally:
            j = j + 1
            time.sleep(1)


def test_page_redirect_class(driver2,class2,after_url,error_info):  
    before_url = driver2.current_url
    before_handle =  driver2.current_window_handle
    flag = False
    flag1 = False
    j = 0
                        
    #  target attribute indicates that page will redirect to external web page   
    if driver2.find_element_by_class_name(class2).get_attribute('target') == u'_blank':
        driver2.find_element_by_class_name(class2).click()
        time.sleep(2) 
        while not flag1:
            driver2.switch_to_window(driver2.window_handles[-1])
            if before_handle != driver2.current_window_handle:
                flag1 = True  
    else:
        driver2.find_element_by_class_name(class2).click()
        time.sleep(2) 
                            
    while not flag and j < 15:        
        try:
            if driver2.find_element_by_xpath("//body").text == error_info:
                flag = True
                print "site cannot be reached"
                return flag
            
            elif driver2.current_url == after_url:
                after_url = driver2.current_url
                flag = True
                print 'web page redirects successfully'
                return flag
        
            elif driver2.current_url == before_url:    
                print "new page is not present now,wait some seconds"                

           
        except:
            pass
        
        finally:
            j = j + 1
            time.sleep(2)
     
def test_page_redirect_link(driver2,link2,after_url,error_info): 
    before_url = driver2.current_url
    before_handle =  driver2.current_window_handle
    print before_handle
    flag = False
    flag1 = False
    i = 0
    j = 0
#  error_info = u'This site can\u2019t be reached\nwww.youtube.com took too long to respond.\nERR_CONNECTION_TIMED_OUT\nReload\nDetails'          
                   
    #  target attribute indicates that page will redirect to external web page
    expression = driver2.find_element_by_link_text(link2).get_attribute('target') == u'_blank' or 'facebook' in driver2.find_element_by_link_text(link2).get_attribute('class') or 'twitter' in driver2.find_element_by_link_text(link2).get_attribute('class')     
    if expression:

        driver2.find_element_by_link_text(link2).click()
        time.sleep(2)  
        while not flag1 and i < 10:
            driver2.switch_to_window(driver2.window_handles[-1])
            time.sleep(2)
            if before_handle != driver2.current_window_handle:
                print driver2.current_window_handle
                flag1 = True               
    else:
        driver2.find_element_by_link_text(link2).click()
        time.sleep(2)  
     
    while not flag and j < 15: 
        print driver2.current_url
    
        try:
            if driver2.current_url == after_url:
                after_url = driver2.current_url
                flag = True
                print 'web page redirects successfully'
                return flag
            
            elif driver2.find_element_by_xpath("//body").text == error_info:
                flag = True
                print "site cannot be reached"
                return flag
        
            elif driver2.current_url == before_url:   
                print "new page is not present now,wait some seconds" 
           
        except:
            pass
        finally:
            time.sleep(2)
            j = j + 1

def test_share_by_facebook(driver2,xpath2,after_url,error_info):  
    before_url = driver2.current_url
    before_handle =  driver2.current_window_handle
    flag = False
    flag1 = False
#   error_info = u'This site can\u2019t be reached\nwww.facebook.com took too long to respond.\nERR_CONNECTION_TIMED_OUT\nReload\nDetails'                        
    #  target attribute indicates that page will redirect to external web page   
    driver2.find_element_by_xpath(xpath2).click()
    
    time.sleep(2) 
    while not flag1:
        
        driver2.switch_to_window(driver2.window_handles[-1])
        if before_handle != driver2.current_window_handle:
                
            flag1 = True  

            
                           
    while not flag:   
        print driver2.current_url    
        print after_url
        try:
            if driver2.find_element_by_xpath("//body").text == error_info:
                flag = True
                print "site cannot be reached"
                return flag
            
            elif driver2.current_url == after_url:

                flag = True
                print 'web page redirects successfully'
                return flag
        
            elif driver2.current_url == before_url:    
                print "new page is not present now,wait some seconds"                

           
        except:
            pass


            
def test_play_video(driver,ele,xpath_iframe,error_id):
        # move to play button and click
        ActionChains(driver).move_to_element(ele).perform()
        ele.click()
        flag = False
        # verify that video frame is present
        try:
            while not flag:
                test_present_and_wait_xpath(driver,xpath_iframe, 5)
                frame1 = driver.find_element_by_xpath(xpath_iframe)
                driver.switch_to_frame(frame1)
                flag = True
        except:
            
            print "video frame is not present"
            
        finally:
            try:
                if test_present_and_wait_id(driver,"player",5):
                    return True
                elif driver.find_element_by_id(error_id).is_displayed():
                    return False

  
            except:
                pass



def test_read_csv(filename):
        
    csvfile = file(filename, 'rb')
 
    reader = csv.reader(csvfile)
 
    list1 =[]
    for line in reader:    
        list1.append(line)  
    csvfile.close()


    data = {}
    for i in range(0,len(list1)):
        if i == 0:
            for j in range(0,len(list1[0])):
                data[list1[0][j]] = []

        else:
        
            for key in data.keys():
                n = list1[0].index(key)
                data[key].append(list1[i][n])
    return data       

def test_compare_ele_attribute(data,driver):
    
    for element in data['element']:
        i = data['element'].index(element)
        try:
            test_present_and_wait_xpath(driver,data['xpath'][i],1)   
            ele = driver.find_element_by_xpath(data['xpath'][i])         
        except:
            print element + " element not found"
        
        
        for key in data.keys():
            if key in ('font-family','font-weight','font-size','line-height'):
                try:
                    value = ele.value_of_css_property(key)
                    assert value == data[key][i]
                    print key + " of " + element + " is " + value
                except:
                    print key + " of " + element + " is " + value + "is not equal to expected " + data[key][i]
                    pass
            elif key == 'text':
                try:
                    value = ele.text
                    if compare_string(value,data[key][i]) == True:
                        print key + " of " + element + " is as expected " + value
                except:
#                    
#                    print n
                    pass
                    
            elif key in ('width','height'):
                try:
                    value = ele.size
                    assert value[key] == int(data[key][i])                    
                    print key + " of " + element + " is " +str(value[key])
                except:
                    print key + " of " + element + " is " + str(value[key]) +  " it is not equal to expected " + data[key][i]
                    pass
                    
                    
def compare_string(str1,str2):  # compare length of str
    string1 = []
    string2 = []     
    for m in range(0,len(str1)):
        if str1[m] in string.printable and str2[m] in string.printable:
            try:
                assert str1[m] == str2[m]
            except:
                print "Failed: "
                for r in range(0,m+1):
                    string1.append(str1[r])
                    string2.append(str2[r])
                print string1,string2
                return False
               

        else:        
            try:
                assert ord(str1[m]) == ord(str2[m])
            except:
                print "Failed: "
                for r in range(0,m+1):
                    string1.append(str1[r])
                    string2.append(str2[r])
                print string1,string2
                return False

    return True     

                
            
                       
                
            
                
            