# -*- coding: utf-8 -*-
'''
Created on Apr 25, 2016

@author: Olivia.Xie
'''
import unittest
from PublicFunctions import *
from selenium import webdriver
from selenium.webdriver.support.ui import Select


import setname
import getname

#dev:10.148.60.55
#lpguest
#L0w3Pr0f3r0
#lpguest
#M0r3thAnm33t5thEy3
# staging: http://staging.millenniumhotels.com/en/

class OperationsAfterLogin(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        setname.set_browser_name()
        browser = getname.get_browser_name()
        self.driver = browser
        self.base_url = "https://www.millenniumhotels.com/en"
               
#        self.verificationErrors = []
        self.accept_next_alert = True
        
    def test_footer_wrapper(self):

        driver = self.driver
        driver.get(self.base_url + "/")     
        driver.maximize_window()         
        try:
            
            test_present_and_wait_id(driver,"main-footer-wrapper",1)  #wait for footer
            driver.execute_script("window.scrollTo(0, document.body.scrollHeight)")# scroll down to foofer
            driver.find_element_by_xpath("//*[@id='main-footer-wrapper']/section/div[3]/div/article[1]/div/h6").click() #click on "About Us" to expand footer
            
            test_present_and_wait_xpath(driver,"//*[@id='main-footer-wrapper']/section/div[3]/div/article[1]/div/p",1) # wait for list after clicking

            ele = driver.find_element_by_class_name("inner")
            path = "./Libs/actual_footer_wrapper.jpg"
            test_compare_image_without_header(driver,ele,path)
            
        except:
            print "element not found"
            test_difference_between_image(driver,path)
            
        finally:
            attr = driver.find_element_by_xpath("//*[@id='main-footer-wrapper']/section/div[3]/div/article[1]/div/p").get_attribute('style') # to make sure wrapper expands
            self.assertEquals(attr,'display: block;',"list is not displayed")

    def test_login(self):
        driver = self.driver
        driver.get(self.base_url + "/")       
        driver.maximize_window()        
        try:
            
            test_present_and_wait_xpath(driver,"//*[@class='nav']/div/div/p",1) # wait for "SIGN IN"
            driver.find_element_by_xpath("//*[@class='nav']/div/div/p").click() 
            driver.find_element_by_id("login_username").send_keys("olivia.xie@mullenloweprofero.com") #type username
            driver.find_element_by_id("login_password").send_keys("auto12345")   # type password
            driver.find_element_by_id("opt-sign-in").click()
            test_present_and_wait_xpath(driver,"//div[@class='nav']/div/div/div/a",1)  # wait for username
            
        except:
            print "login failed"

        finally:        
            
            userinfo = driver.find_element_by_xpath("//div[@class='nav']/div/div/div/a").text
            self.assertEquals(userinfo,"Hello\nRegression","login failed")
            print userinfo + " login successfully" 
            
            
    def test_offers(self):
        driver = self.driver
        driver.get(self.base_url + "/offers")    
        driver.maximize_window()                  

#        test_present_and_wait_xpath(driver,"/html/body/div[1]/div[3]/div[2]/div/ul/li[5]/a",1) #wait for Offers item in Global menu       
#        driver.find_element_by_xpath("/html/body/div[1]/div[3]/div[2]/div/ul/li[5]/a").click() #click on offers
        test_present_and_wait_class(driver,"tpl-hero-offer",1)   # wait for the hero image in Offers Landing page
        ele = driver.find_element_by_class_name("tpl-hero-offer")
        
        try:
            test_compare_image_without_header(driver,ele,"./Libs/actual_offers.jpg")   # compare the hero image  in Offers landing page            
        except:
            test_difference_between_image(driver,"./Libs/actual_offers.jpg")
            print "hero image is different with expect"
     
        driver.find_element_by_xpath("/html/body/div[1]/div[5]/div/div[3]/div[1]/div[3]/div[1]/div").click()  #click on select                                                         
        test_present_and_wait_class(driver,"destination-list",1)
          
        try:
            test_compare_image_without_header(driver,driver.find_element_by_class_name("contents"),"./Libs/actual_contents.jpg")            
        except:
            test_difference_between_image(driver, "./Libs/actual_contents.jpg")
            
        driver.find_element_by_link_text("New York").click()  #select "New York" in the destination list               
        test_present_and_wait_xpath(driver,"/html/body/div[1]/div[5]/div/div[3]/div[1]/div[3]/div[2]/div[1]",1)
        driver.find_element_by_xpath("/html/body/div[1]/div[5]/div/div[3]/div[1]/div[3]/div[2]/div[1]").click()  # select "Broadway Hotel"
        driver.find_element_by_xpath("/html/body/div[1]/div[5]/div/div[3]/div[1]/div[3]/div[2]/div[2]/ul/li[2]").click() 
               
        try:
            test_compare_image_without_header(driver,driver.find_element_by_class_name('tpl-tours-filter'),"./Libs/actual_select.jpg")
        except:    
            test_difference_between_image(driver, "./Libs/actual_select.jpg")  
              
                                             
        test_present_and_wait_xpath(driver,"/html/body/div[1]/div[5]/div/div[4]/div/div[1]/div[1]",1)
        txt1 = driver.find_element_by_xpath("/html/body/div[1]/div[5]/div/div[4]/div/div[1]/div[1]/div[1]/div[2]/h3/span[1]").text
        ele1 = driver.find_element_by_xpath("/html/body/div[1]/div[5]/div/div[4]/div/div[1]/div[1]")        
        test_image_grab_without_header(driver, ele1, "./Libs/actual_us.jpg")               
                                      
        driver.find_element_by_xpath("/html/body/div[1]/div[5]/div/div[4]/div/div[1]/div[1]/div[1]/div[2]/div/a").click()  # EXPLORE
                                             
        test_present_and_wait_xpath(driver,"/html/body/div[1]/div[5]/div/div[2]/div[1]/div[2]/div/p",1)
        txt2 = driver.find_element_by_xpath("/html/body/div[1]/div[5]/div/div[2]/div[1]/div[2]/div/p").text  #text over image                                     
        ele3 = driver.find_element_by_xpath("/html/body/div[1]/div[5]/div/div[2]/div[1]")
        test_image_grab_without_header(driver,ele3,"./Libs/actual_offers1.jpg")                              

        test_image_grab_without_header(driver,driver.find_element_by_xpath("/html/body/div[1]/div[5]/div/div[2]/div[3]"),"./Libs/actual_offer_details.jpg")

        test_image_grab_without_header(driver,driver.find_element_by_xpath("/html/body/div[1]/div[5]/div/div[2]/div[6]"),"./Libs/actual_similar_offers.jpg")
  
        try:
            test_compare_image_without_header(driver,driver.find_element_by_xpath("/html/body/div[1]/div[5]/div/div[2]/div[4]/div[1]/div[1]"),"./Libs/actual_seamless.jpg")                               
                            
        except:
            test_difference_between_image(driver,"./Libs/actual_seamless.jpg")    
        finally:
            self.assertEquals(txt1,txt2,"failed to open the selected offer in us")
            
                
    def test_scroll_down(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.maximize_window()
        try:
            
            test_present_and_wait_class(driver,"wgt-scrolldown",1) # wait for"SCROLL TO EXPLORE" 
            driver.find_element_by_class_name("wgt-scrolldown").click()
            test_present_and_wait_xpath(driver,"/html/body/div[1]/div[5]/div[2]/div",1) #wait for the featured image related with current user
        except:
            print "element not found"
        finally:
            ele = driver.find_element_by_xpath("/html/body/div[1]/div[5]/div[2]/div")
            if driver.name == 'chrome':
                y_offset = 61
            elif driver.name == 'internet explorer':
                y_offset = 55
            elif driver.name == 'firefox':
                y_offset = 97
            im1 = ImageGrab.grab((int(ele.location['x']),y_offset,int(ele.location['x']+ele.size['width']),int(ele.size['height'])+y_offset))
            im1.save("./Libs/image1.jpg")
            list1 = im1.histogram()
        
        # move to image by executing java script, and then compare it with the one captured just now

        driver.execute_script("arguments[0].scrollIntoView(true);", ele)
#        driver.execute_script("window.scrollBy(0," + str(ele.size['height']) + ")") 
        im2 = ImageGrab.grab((int(ele.location['x']),y_offset,int(ele.location['x']+ele.size['width']),int(ele.size['height'])+y_offset))
        im2.save("./Libs/image2.jpg")       
        list2 = im2.histogram()  # take a screenshot after moving to the desired element
        self.assertEquals(list1,list2,"SCROLL TO EXPLORE button does not work as expected") 
                 
                                
    def test_login_and_booking(self):        
        driver = self.driver
        driver.get(self.base_url + "/")
    
        driver.maximize_window() 
        path = "./Libs/actual_bar.jpg"
        path1 = "./Libs/actual_destination.jpg"
        path2 = "./Libs/actual_checkout.jpg"
        global number
        try:  #click on booking bar and capture screenshot            
            test_present_and_wait_class(driver,"booking-bar",1)
            ele = driver.find_element_by_class_name("booking-bar")
            try:
                test_compare_image_without_header(driver,ele,path)    
            except:
                test_difference_between_image(driver,path)        
        except:                       
            print "booking bar is inconsistent with expected image"
                       
        try: #  booking dialog
            ele.click()                 
            test_present_and_wait_class(driver,"booking",1)
            ele1 = driver.find_element_by_class_name('booking')
            try:
                test_compare_image_without_header(driver,ele1,path1)
            except:              
                test_difference_between_image(driver,path1)   
        except:                
            print "destination frame is inconsistent with expected image"   
                
        try:  #input destination
            driver.find_element_by_xpath("/html/body/div[1]/div[2]/div/div[2]/div[1]/div[1]/input").send_keys("Tara") 
            test_present_and_wait_xpath(driver,"//*[@class='booking-destination-list']/ul/li",1)
        except:
            print "destination list is not displayed"
                   
        try:# select desired hotel
            driver.find_element_by_xpath("//*[@class='booking-destination-list']/ul/li").click() 
            test_present_and_wait_xpath(driver,"//*[@id='younger']/div/div/table/tbody/tr[2]/td[1]/span",1)
        except:
            print "date is not displayed"
                    
        try: #select date and click on Search; wait for rooms page
            for i in range(0,4):
                driver.find_elements_by_id("nextMonth")[1].click()                                                          
            driver.find_element_by_xpath("//*[@id='younger']/div/div/table/tbody/tr[2]/td[1]/span").click() #select date
            driver.find_element_by_xpath("/html/body/div[1]/div[2]/div/div[3]/a").click()  #click on Search
            driver.find_element_by_link_text("ROOMS").click()
            driver.execute_script("window.scrollBy(0,200)")
            test_present_and_wait_class(driver,"wgt-sqbutton-blue",1) 
#            test_present_and_wait_xpath(driver,"//*[@id='room-items-wrapper']/div/div[3]/div[1]/div[2]/div[4]/div[2]/a",1)

        except:
            print "Book Room button is not displayed"
                            
                          
        try: # click on book room and wait for "SELECT AND CUSTOMISE"     
            driver.find_elements_by_class_name("wgt-sqbutton-blue")[1].click()                          
#            driver.find_element_by_xpath("//*[@id='room-items-wrapper']/div/div[3]/div[1]/div[2]/div[4]/div[2]/a").click() #click on BOOK ROOM
            test_present_and_wait_xpath(driver,"//*[@id='choose-rate-wrapper']/div/div[2]/ul/li[2]/div[2]/div[2]/a",2) 
                                          
#            driver.execute_script("window.scrollBy(0,460)")                  
        except:
            print "SELECT AND CUSTOMISE is not displayed"
                            
        try: #click on "SELECT AND CUSTOMISE" and then click on Submit button to go on
            
            driver.find_element_by_xpath("//*[@id='choose-rate-wrapper']/div/div[2]/ul/li[2]/div[2]/div[2]/a").click()
            try:
                test_present_and_wait_class(driver,"wgt-sqbutton-red",1)
                driver.find_element_by_class_name("wgt-sqbutton-red").click()
    #            test_present_and_wait_id(driver,"submit",2)
    #            driver.find_element_by_id("submit").click()
                
            except:
                print "already click on select and customize and wait for the checkout button"
            test_present_and_wait_xpath(driver,"//*[@id='review-wrapper']/div[1]/div/div[2]/ul/li[1]",1)
            print "checkout button in bed type tab is displayed"
            
        except:
            print "CHECKOUT button is not displayed"
                                    
                                
        try: #capture the header to make sure booking is successful
            test_present_and_wait_css(driver,"li.bookingitem-header",1)
            ele2 = driver.find_element_by_css_selector("li.bookingitem-header")  
#            test_present_and_wait_xpath(driver,"//*[@id='review-wrapper']/div[3]/div/div[2]/div[5]/a",2)
#            ele2 = driver.find_element_by_xpath("//*[@id='review-wrapper']/div[1]/div/div[2]/ul/li[1]") 
            try:
                test_compare_image_without_header(driver, ele2, path2) 
            except:
                test_difference_between_image(driver,path2)                                 
                                                                                    
        except:
            test_difference_between_image(driver, path2)
            print "NEXTSTEP is not displayed"   
                                    
        try: # click on NEXTSTEP to wait for the enter payment info
                                          
            driver.find_element_by_xpath("//*[@id='review-wrapper']/div[5]/div/div[2]/div[5]/a").click()
            test_present_and_wait_name(driver,"name",1)
            
        except:
            print "name input field is not displayed"
                                        
        try: #enter your details and click on BOOKNOW
            time.sleep(10)
            driver.find_element_by_name("name").send_keys("TEST")
            driver.find_element_by_name("number").send_keys("1111222233334444")

            driver.find_element_by_name("security_code").send_keys("123")
            select = Select(driver.find_element_by_name("mm"))
            select.select_by_visible_text("01")
            select.select_by_visible_text("10")
            select1 = Select(driver.find_element_by_name("yy"))
            select1.select_by_visible_text("2018")
            driver.execute_script("window.scrollBy(0,200)")
            time.sleep(1)
            driver.find_element_by_class_name("wgt-sqbutton-blue").click()
            time.sleep(15)
#            driver.find_element_by_id("next-step").click()
            test_present_and_wait_css(driver,"li.bookingitem-header",1)
            ele3 = driver.find_element_by_css_selector("li.bookingitem-header")           
            path3 = "./Libs/actual_confirmed.jpg"
            try:
                test_compare_image_without_header(driver, ele3, path3)
            except:
                test_difference_between_image(driver,path3)                                                
        except:
            print "booking item header is not displayed"            
            
        finally:
            number = driver.find_element_by_xpath("//*[@id='booking-item-list-wrapper']/div[1]/div[1]/div[2]/ul/li[2]/p/em").text
            txt = driver.find_element_by_xpath("/html/body/div[1]/div[6]/div[1]/div[2]/div/div/div[1]/div/p").text
            self.assertEquals(txt,"Your reservation is confirmed!","reservation failed")
            print number
            return number  # order id


    def test_login_cancel_booking(self):
        driver = self.driver
        driver.get(self.base_url + "/view-or-modify-your-booking/")   
        driver.maximize_window()  
        
        try:  # input last name and order id            
            test_present_and_wait_name(driver,"last_name",1)
            driver.find_element_by_name("last_name").send_keys("Automation")
            driver.find_element_by_name("confirmation_number").send_keys(number)  
            driver.find_element_by_xpath("/html/body/div[1]/div[6]/div/div/div[3]/a").click()
            test_present_and_wait_xpath(driver,"//*[@id='review-wrapper']/div[1]/div[2]/a",1)
        except:
            print "CANCEL button is not displayed"
            
        try:  #click on Cancel and confirm cancellation,//*[@id="review-wrapper"]/div[1]/div[2]/a
            try:
                test_compare_image_without_header(driver,driver.find_element_by_class_name("bookingitem-header"),"./Libs/actual_cancel.jpg") 
            except:
                test_difference_between_image(driver,"./Libs/actual_cancel.jpg")
            driver.find_element_by_xpath("//*[@id='review-wrapper']/div[1]/div[2]/a").click()
            test_present_and_wait_xpath(driver,"//*[@id='removeConfirmWrapper']/div/div/a[2]",1)
            driver.find_element_by_xpath("//*[@id='removeConfirmWrapper']/div/div/a[2]").click()
        except:
            print "cancellation dialog is not displayed"                
    
        try:  # assert feedback of cancellation
            test_present_and_wait_xpath(driver,"/html/body/div[1]/div[6]/div[1]/div[3]/div/div/div[1]/div/h4",1)
            try:
                test_compare_image_without_header(driver,driver.find_element_by_class_name("bookingitem-header"),"./Libs/actual_cancelled.jpg")
            except:
                test_difference_between_image(driver,"./Libs/actual_cancelled.jpg")
            txt = driver.find_element_by_xpath("/html/body/div[1]/div[6]/div[1]/div[3]/div/div/div[1]/div/h4").text
            print txt
            
        except:
            pass
            print "no cancellation feedback"
            
        finally:
            self.assertEquals(txt,"Your booking has been cancelled","cancellation failed")  

    def test_map(self):
        driver = self.driver
        driver.get(self.base_url + "/destinations")      
        driver.maximize_window()      
        path = "./Libs/actual_world_map.jpg"
        path1 = "./Libs/actual_asia_map.jpg"
        path2 = "./Libs/actual_china_map.jpg"
        path3 = "./Libs/actual_beijing_map.jpg" 
        path4 =  "./Libs/actual_hotel_map.jpg"
        # world map
#        test_present_and_wait_xpath(driver,"/html/body/div[1]/div[3]/div[2]/div/ul/li[4]/a",1)
#        driver.find_element_by_xpath("/html/body/div[1]/div[3]/div[2]/div/ul/li[4]/a").click()
        test_present_and_wait_id(driver,"map",1)
        ele = driver.find_element_by_id("map")
        try:
            test_compare_image_without_header(driver,ele,path)
            flag = True
        except:
            print "world map is inconsistent with expected one"
            flag = False
            test_difference_between_image(driver,path)
        # Asia map                    
        driver.find_element_by_xpath("//*[@id='map']/div/div/div[1]/div[4]/div[3]/div[2]/div[1]").click()
        test_present_and_wait_id(driver,"map",1)
        try:
            test_compare_image_without_header(driver, ele, path1) 
            flag1 = True    
        except:
                print "asia map is inconsistent with expected one"
                flag1 = False
                test_difference_between_image(driver,path1)
        # China map                    
        driver.find_element_by_xpath("//*[@id='map']/div/div/div[1]/div[4]/div[3]/div[1]/div[1]").click()
        test_present_and_wait_id(driver,"map",1)
        try:
            test_compare_image_without_header(driver, ele, path2) 
            flag2 = True
        except:
            flag2 = False
            print "china map is inconsistent with expected one"
            test_difference_between_image(driver,path2)
        #Chengdu map
        driver.find_element_by_xpath("//*[@id='map']/div/div/div[1]/div[4]/div[3]/div[1]/div[1]").click()
        test_present_and_wait_id(driver,"map",1)
        try:
            test_compare_image_without_header(driver, ele, path3) 
            flag3 = True
        except:
            flag3 = False
            print "Beijing map is inconsistent with expected one"
            test_difference_between_image(driver,path3)
        #Beijing hotel                 
        driver.find_element_by_xpath("//*[@id='map']/div/div/div[1]/div[4]/div[3]/div[1]/div[1]").click()
        test_present_and_wait_xpath(driver,"//*[@id='DestinationInformation']/div/ul/li[1]/div[2]/div[1]/div/div/div[1]/div[2]",1)
        try:
            test_compare_image_without_header(driver,ele,path4)
            flag4 = True
        except:
            flag4 = False
            print "Millennium Hotel Beijing is inconsistent with expected one"
            test_difference_between_image(driver,path4)
        finally:
            Flag =  flag and flag1 and flag2 and flag3 and flag4        
            self.assertEquals(Flag,True,"some maps are inconsistent with corresponding expect images")

                   
    def test_user_logout(self):

        driver = self.driver
        driver.get(self.base_url + "/")       
        driver.maximize_window()
        try: 
                
                test_present_and_wait_xpath(driver,"//*[@class='nav']/div/div/div/a[2]",1) # wait for "Sign out"
                driver.find_element_by_xpath("//*[@class='nav']/div/div/div/a[2]").click() # click button to logout
                test_present_and_wait_xpath(driver,"//*[@class='nav']/div/div/h6",1)
                txt1 = driver.find_element_by_xpath("//*[@class='nav']/div/div/h6").text
        except:
                print "element not found"                
        finally:                       
                self.assertEquals(txt1,"SIGN IN","logout failed")
                print " logout successfully"     
                
      

    @classmethod
    def tearDownClass(self):
        self.driver.quit()
#        self.assertEqual([], self.verificationErrors)
if __name__ == "__main__":
    unittest.main()
