# -*- coding: utf-8 -*-
'''
Created on Apr 25, 2016

@author: Olivia.Xie
'''
import unittest
from PublicFunctions import *
from selenium import webdriver
import random
from selenium.webdriver.support.ui import Select
import setname
import getname
import time

class BookingWithoutLogin(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        setname.set_browser_name()
        browser = getname.get_browser_name()
        self.driver = browser
        self.base_url = "https://www.millenniumhotels.com/en"        
#        self.verificationErrors = []
        self.accept_next_alert = True

        
    def test_logout_booking(self):
        
        driver = self.driver
        driver.get(self.base_url + "/")

        driver.maximize_window() 
        path = "./Libs/actual_logout_bar.jpg"
        path1 = "./Libs/actual_logout_destination.jpg"
        path2 = "./Libs/actual_logout_checkout.jpg"
        global number
        try:  #click on booking bar and capture screenshot

            test_present_and_wait_class(driver,"booking-bar",1)
            ele = driver.find_element_by_class_name("booking-bar")
            try:
                test_compare_image_without_header(driver,ele,path)
            except:
                test_difference_between_image(driver,path)
            
        except:                       
            print "booking bar is inconsistent with expected image"
 
            
        try: #  booking dialog
            ele.click()             
            test_present_and_wait_xpath(driver,"/html/body/div[1]/div[2]/div",1)
            ele1 = driver.find_element_by_xpath("/html/body/div[1]/div[2]/div")
            try:
                test_compare_image_without_header(driver,ele1,path1)
            except:
                test_difference_between_image(driver,path1)
                   
        except:                
                print "destination frame is inconsistent with expected image" 

        try:  #input destination
            driver.find_element_by_xpath("/html/body/div[1]/div[2]/div/div[2]/div[1]/div[1]/input").send_keys("Tara") 
            test_present_and_wait_xpath(driver,"//*[@class='booking-destination-list']/ul/li",1)
            
        except:    
            print "destination list is not displayed"
        
        try: # select desired hotel
            driver.find_element_by_xpath("//*[@class='booking-destination-list']/ul/li").click() 
            test_present_and_wait_xpath(driver,"//*[@id='younger']/div/div/table/tbody/tr[5]/td[1]/span",1)

        except:
            print "date is not displayed"
                    
        try: #select date and click on Search; wait for rooms page
            for i in range(0,4):
                driver.find_elements_by_id("nextMonth")[1].click()                                            
            driver.find_element_by_xpath("//*[@id='younger']/div/div/table/tbody/tr[2]/td[1]/span").click() #select date
            driver.find_element_by_xpath("/html/body/div[1]/div[2]/div/div[3]/a").click()  #click on Search
            driver.find_element_by_link_text("ROOMS").click()
            driver.execute_script("window.scrollBy(0,200)")  
            test_present_and_wait_class(driver,"wgt-sqbutton-blue",1)        
#            test_present_and_wait_xpath(driver,"//*[@id='room-items-wrapper']/div/div[3]/div[1]/div[2]/div[4]/div[2]/a",1)
        except:
            print "Book Room button is not displayed"
                        
        try: # click on book room and wait for "SELECT AND CUSTOMISE"
            driver.find_elements_by_class_name("wgt-sqbutton-blue")[1].click()
            driver.find_element_by_xpath("//*[@id='room-items-wrapper']/div/div[3]/div[1]/div[2]/div[4]/div[2]/a").click() #click on BOOK ROOM
            test_present_and_wait_xpath(driver,"//*[@id='choose-rate-wrapper']/div/div[2]/ul/li[2]/div[2]/div[2]/a",1)
#            driver.execute_script("window.scrollBy(0,460)","")                                
        except:
            print "SELECT AND CUSTOMISE is not displayed"
                                
        try: #click on "SELECT AND CUSTOMISE" and then click on Submit button to go on

            driver.find_element_by_xpath("//*[@id='choose-rate-wrapper']/div/div[2]/ul/li[2]/div[2]/div[2]/a").click()
            try:
                test_present_and_wait_class(driver,"wgt-sqbutton-red",1)
                driver.find_element_by_class_name("wgt-sqbutton-red").click()
            except:
                print "click on select and customize and wait for the checkout button"
            test_present_and_wait_xpath(driver,"//*[@id='review-wrapper']/div[1]/div/div[2]/ul/li[1]",1)
            print "checkout button in bed type tab is displayed"
        except:                                    
            print "CHECKOUT button is not displayed"
                                    
                                
        try: #capture the header to make sure booking is successful
            test_present_and_wait_css(driver,"li.bookingitem-header",1)
            ele2 = driver.find_element_by_css_selector("li.bookingitem-header") 
#            test_present_and_wait_xpath(driver,"//*[@id='review-wrapper']/div[3]/div/div[2]/div[5]/a",1)
#            ele2 = driver.find_element_by_xpath("//*[@id='review-wrapper']/div[1]/div/div[2]/ul/li[1]")    
            try:
                test_compare_image_without_header(driver, ele2, path2) 
            except:
                test_difference_between_image(driver,path2)                                    
                                                                                             
        except:
            print "NEXTSTEP is not displayed"  
                                        
        try: # click on "Check out without registering with us" to enter user details
            driver.find_element_by_xpath("//*[@id='review-wrapper']/div[5]/div/div/div[5]/a[1]").click()
            test_present_and_wait_name(driver,"first_name",1)

        except:
            print "name input field is not displayed"
            
        try: #enter your details and click on BOOKNOW
            global last_name
            last_name = string.join(random.sample(['a','b','c','d','e','f','g','h','i','j','h'], 4)).replace(" ","")
            driver.find_element_by_name("first_name").send_keys("Regression") #first name
            driver.find_element_by_name("last_name").send_keys(last_name) #last name
            driver.find_element_by_name("email").send_keys("olivia.xie@mullenloweprofero.com")  #email address
                                                
            select = Select(driver.find_element_by_name("title"))  # select parent element
            select.select_by_visible_text('Mrs.')
                                                
            driver.find_element_by_name('phone').send_keys('1010101010')                                                
            driver.find_element_by_name("address").send_keys("JinTai Rd")
            driver.find_element_by_name("city").send_keys("Beijing")
                                                
            select1 = Select(driver.find_element_by_name('country'))
            select1.select_by_visible_text('China')
                                                
            driver.find_element_by_name('state').send_keys('Beijing')
            driver.find_element_by_name('postal').send_keys('100086')
                                    
            driver.find_element_by_name("name").send_keys("TEST")
            driver.find_element_by_name("number").send_keys("1111222233334444")

            driver.find_element_by_name("security_code").send_keys("123")
            select = Select(driver.find_element_by_name("mm"))
            select.select_by_visible_text("01")
            select.select_by_visible_text("10")
            select1 = Select(driver.find_element_by_name("yy"))
            select1.select_by_visible_text("2018")
            time.sleep(1)                                                     
            book_now = driver.find_element_by_id("next-step")                                     
            ActionChains(driver).move_to_element(book_now).perform()   
            driver.find_element_by_class_name("wgt-sqbutton-blue").click()                                                                                            
#            driver.find_element_by_id("next-step").click()     # IE   
            time.sleep(15)    
            test_present_and_wait_css(driver,"li.bookingitem-header",1)
            ele3 = driver.find_element_by_css_selector("li.bookingitem-header")                                  
#            test_present_and_wait_class(driver,"bookingitem-header",2)
#            ele3 = driver.find_element_by_class_name("bookingitem-header")           
            try:
                test_compare_image_without_header(driver, ele3, "./Libs/actual_logout_confirmed.jpg")
            except:
                test_difference_between_image(driver,"./Libs/actual_logout_confirmed.jpg")                                                
        except:
            print "booking item header is not displayed"

        finally:                                                
            number = driver.find_element_by_xpath("//*[@id='booking-item-list-wrapper']/div[1]/div[1]/div[2]/ul/li[2]/p/em").text
            txt = driver.find_element_by_xpath("/html/body/div[1]/div[6]/div[1]/div[2]/div/div/div[1]/div/p").text
            self.assertEquals(txt,"Your reservation is confirmed!","reservation failed")
            print number, last_name        
            return number,last_name  # order id and last name used to cancel booking
                                    
        
    def test_logout_cancel_booking(self):
        
        driver = self.driver
        driver.get(self.base_url + "/view-or-modify-your-booking/")
        driver.maximize_window()  
        
        try:  # input last name and order id
            test_present_and_wait_name(driver,"last_name",1)
            driver.find_element_by_name("last_name").send_keys(last_name)
            driver.find_element_by_name("confirmation_number").send_keys(number)  
            driver.find_element_by_xpath("/html/body/div[1]/div[6]/div/div/div[3]/a").click()
            test_present_and_wait_xpath(driver,"//*[@id='review-wrapper']/div[1]/div[2]/a",1)
            try:
                test_compare_image_without_header(driver,driver.find_element_by_class_name("bookingitem-header"),"./Libs/actual_logout_cancel.jpg")
            except:
                test_difference_between_image(driver,"./Libs/actual_logout_cancel.jpg")
        except:
            print "CANCEL button is not displayed"
            
            
        try:  #click on Cancel and confirm cancellation                                
            driver.find_element_by_xpath("//*[@id='review-wrapper']/div[1]/div[2]/a").click()
            test_present_and_wait_xpath(driver,"//*[@id='removeConfirmWrapper']/div/div/a[2]",2)                
            driver.find_element_by_xpath("//*[@id='removeConfirmWrapper']/div/div/a[2]").click()
            
        except:
            print "cancellation dialog is not displayed"
            
        try:  # assert feedback of cancellation 
            test_present_and_wait_xpath(driver,"/html/body/div[1]/div[6]/div[1]/div[3]/div/div[2]/div[1]/div/h4",1)
            txt = driver.find_element_by_xpath("/html/body/div[1]/div[6]/div[1]/div[3]/div/div[2]/div[1]/div/h4").text
            print txt
            try:
                test_compare_image_without_header(driver,driver.find_element_by_class_name("bookingitem-header"),"./Libs/actual_logout_cancelled.jpg")
            except:
                test_difference_between_image(driver,"./Libs/actual_logout_cancelled.jpg")

        except:
            print "no cancellation feedback"
                
        finally:
            self.assertEquals(txt,"Your booking has been cancelled","cancellation failed")                
                             
        
    @classmethod
    def tearDownClass(self):
        self.driver.quit()
#        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
