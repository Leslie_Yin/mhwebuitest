'''
Created on Jun 17, 2016

@author: olivia.xie
'''
#!/usr/bin/env python2.7 

from selenium import webdriver


class GlobalVar:

    browser_name = ''



def set_browser_name(times):
    if times == 0:
        options = webdriver.ChromeOptions()
        options.add_argument("test-type")  
        options.add_argument("start-maximized")
        options.add_argument("--disable-extensions")
        GlobalVar.browser_name = webdriver.Chrome(chrome_options=options)

    if times == 1:
#        profile = webdriver.FirefoxProfile("C:/Users/Olivia.Xie/AppData/Roaming/Mozilla/Firefox/Profiles/2ukqrxnq.MH")        
        GlobalVar.browser_name = webdriver.Firefox()

    if times == 2:
        GlobalVar.browser_name = webdriver.Ie()        
    

def get_browser_name():

    return GlobalVar.browser_name


