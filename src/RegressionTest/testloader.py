#coding=utf-8

import sys
import unittest
import HTMLTestRunner
import time,os,sys
from selenium import webdriver


#注意使用套件时，在单个py文件中下的多个用例用  (类名("方法名")),
#导入多个py的类下，用（py名.类名）


path = "C:\\jenkins\\workspace\\mhwebuitest\\src\\RegressionTest"   #指明要自动查找的py文件所在文件夹路径
# "C:\\Users\\Olivia.Xie\\workspace\\KP_Preview\\src\\TestOnChrome"
# "C:\\Users\\Olivia.Xie\\workspace\\HotelUAT\\src\\RegressionTest"

def createSuite(case_path):  #产生测试套件    
    testunit=unittest.TestSuite()
    #使用discover找出用例文件夹下test_casea的所有用例
    
    discover=unittest.defaultTestLoader.discover(case_path,   #查找的文件夹路径
    pattern="*.py",                                      #要测试的模块名，以start开头的.py文件
    top_level_dir=None)                                  #测试模块的顶层目录，即测试用例不是放在多级目录下，设置为none
    for suite in discover:                               #使用for循环出suite,再循环出case
        for case in suite:
            testunit.addTests(case)
    print testunit
    return testunit


def runSuite(allcasenames):
    now=time.strftime("%Y-%m-%d-%H-%M-%S",time.localtime(time.time()))
    filename=path+"\\Report\\TestReport.html"
    fp=file(filename,"wb")
    runner=HTMLTestRunner.HTMLTestRunner(stream=fp,title=u"Automation_Test_Report_"+now,description=u"ResultForEachCase")
    runner.run(allcasenames)    #执行case
    fp.close()

if __name__ == "__main__":

    allcasenames = createSuite(path)
    runSuite(allcasenames)

    
    


